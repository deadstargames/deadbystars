﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour {


	private UnityEngine.Camera Camera;

	void Start () {
		Camera = FindObjectOfType<UnityEngine.Camera> ();
		Cursor.visible = false;
	}

	void Stop () {
		Cursor.visible = true;
	}

	void Update () {
		var mousePosition = Input.mousePosition;
		var worldPosition = Camera.ScreenToWorldPoint (mousePosition);
		transform.position = (Vector2) worldPosition;
	}
}
