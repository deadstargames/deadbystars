﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Activator : MonoBehaviour
{
    [SerializeField]
    bool AutoInteract = false;

    public bool Active { get; private set; }

    public event Action OnActivated;

    private bool pasued = false;

	void Start ()
    {
		
	}

	void Update ()
    {
        UpdateColor();
        UpdateInteraction();
    }

    void UpdateColor()
    {
        Color targetColor = Color.white;

        if (Active)
            targetColor *= (Mathf.Sin(Time.timeSinceLevelLoad * 5f) + 9f) / 10f;

        GetComponent<Renderer>().material.color = targetColor;
    }

    void UpdateInteraction()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Debug.Log("ESC!");

            this.pasued = !this.pasued;

            if (!this.pasued) {
                var player = FindObjectOfType<Player>();
                player.OnPauseGame();

                var player_shooting = FindObjectOfType<PlayerShooting>();
                player_shooting.OnPauseGame();

                var zombies = FindObjectsOfType<Zombie>() as Zombie[];
                foreach (Zombie zombie in zombies) {
                    zombie.OnPauseGame();
                }

            } else {
                var player = FindObjectOfType<Player>();
                player.OnResumeGame();

                var player_shooting = FindObjectOfType<PlayerShooting>();
                player_shooting.OnResumeGame();

                var zombies = FindObjectsOfType<Zombie>() as Zombie[];
                foreach (Zombie zombie in zombies) {
                    zombie.OnResumeGame();
                }
            }
        }

        if (!Active)
            return;

        if (Input.GetKeyDown(KeyCode.Space) || AutoInteract)
            if (OnActivated != null)
                OnActivated.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!CheckIfPlayer(collision))
            return;

        Active = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!CheckIfPlayer(collision))
            return;

        Active = false;
    }

    bool CheckIfPlayer(Collider2D collider)
    {
        return (collider.gameObject.GetComponent<Player>() != null);
    }
}
