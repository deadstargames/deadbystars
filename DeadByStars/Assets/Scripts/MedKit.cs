﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Activator))]
public class MedKit : MonoBehaviour
{
    public int Amount = 10;

    void Start()
    {
        GetComponent<Activator>().OnActivated += () =>
        {

            FindObjectOfType<Player>().GetComponent<Entity>().Health += Amount;
           
            Destroy(gameObject);
        };
    }
}
