﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour {

	private int InitZombieCount = 0;
	private int KilledZombie = 0;
	private bool Success = false;

	[SerializeField]
	public GameObject gameMenu;

	[SerializeField]
	public string NextSceneName;

	void Start () {
		var zombies = FindObjectsOfType<Zombie> () as Zombie[];
		this.InitZombieCount = zombies.Length;
	}
	
	// Update is called once per frame
	void Update () {

	}

	private void GameEnd(bool success) {
		print("Game end with " + success);
		this.Success = success;
		this.ShowMenu ();
	}

	public static GameState Instance() {
		return FindObjectOfType<GameState> ();
	}

	private void TestGameEnd() {
		if (this.KilledZombie >= this.InitZombieCount) {
			this.GameEnd (true);
		}
	}

	void zombieKill() {
		this.KilledZombie++;
		this.TestGameEnd ();
		print ("Zombie killed");
	}

	public void killGameObject(GameObject killedGameObject) {
		if (killedGameObject.tag == "Zombie") {
			this.zombieKill ();
		}

		if (killedGameObject.tag == "Player") {
			this.GameEnd(false);
		}
	}

	private void ShowMenu() {
		Instantiate (gameMenu);
	}

	public bool GetEndGameStatus() {
		return this.Success;
	}

	public int GetKilledZombieCount() {
		return this.KilledZombie;
	}

	public string GetNextSceneName() {
		return NextSceneName;
	}
}
