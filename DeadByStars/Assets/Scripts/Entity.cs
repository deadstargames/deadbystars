﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [SerializeField]
    float InitialHealth = 3f;

	private GameState State;

    private float health;
    public float Health {
        get {
            return health;
        }

        set {
            health = value;

            if (health <= 0)
                health = 0;

            if (OnHealthChanged != null)
                OnHealthChanged.Invoke(health);

            if (health == 0) {
                if (OnKilled != null)
                    OnKilled.Invoke();


				this.State.killGameObject (gameObject);
                Destroy(gameObject);
            }   
        }
    }

    public event Action<float> OnHealthChanged;
    public event Action OnKilled;

	void Start () {
        Health = InitialHealth;
		this.State = GameState.Instance ();
    }

    public void addHealth(int ammount) {
        health += ammount;
    }
}
