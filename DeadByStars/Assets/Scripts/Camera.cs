﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {
	
	Rigidbody2D PlayerRigidbody2D;
	Player Player;
    UnityEngine.Camera MainCamera;

	void Start () {
		Player = FindObjectOfType<Player> ();
		PlayerRigidbody2D = Player.GetComponent<Rigidbody2D>();
        MainCamera = GetComponent<UnityEngine.Camera>();
	}
	
	void FixedUpdate () {

		if (Player != null) {
			UpdatePosition();
			UpdateFOV();
		}

    }

    void UpdatePosition() {
        var targetPosition =
			(Vector3)PlayerRigidbody2D.position
			+ (Vector3)PlayerRigidbody2D.velocity * 0.5f
            + Vector3.back * 10f;

        transform.position = Vector3.Lerp(
            transform.position,
            targetPosition,
            Time.deltaTime * 4f);
    }

    void UpdateFOV() {
		var speed = PlayerRigidbody2D.velocity.magnitude;
        var targetViewSize = 3f + speed / 2f;

        MainCamera.orthographicSize = Mathf.Lerp(
            MainCamera.orthographicSize,
            targetViewSize,
            Time.deltaTime);
    }
}
