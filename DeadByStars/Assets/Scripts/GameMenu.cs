﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour {

	// Use this for initialization
	private Text Title;
	private Text ZombieCount;
	private Button NextLevelButton;
	private GameState State;

	void Start () {
		Cursor.visible = true;
		Title = GameObject.Find ("Title").GetComponent<Text> ();
		ZombieCount = GameObject.Find ("ZombieCount").GetComponent<Text> ();
		NextLevelButton = GameObject.Find ("NextLevel").GetComponent<Button> ();
		State = GameState.Instance ();

		setupMenu ();
	}

	private void setupMenu() {
		if( State.GetEndGameStatus() ) {
			Title.text = "You are win!";
			if(State.GetNextSceneName() == null ){
				NextLevelButton.gameObject.SetActive (false);
			}
		} else {
			Title.text = "You are lose!";
			NextLevelButton.gameObject.SetActive (false);
		}
		string stringKilled = ("Killed zombie: " + State.GetKilledZombieCount().ToString());
		ZombieCount.text = stringKilled;
	}

	public void PlayAgain()
	{
		var scene = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (scene.name);
	}

	public void NextLevel() {
		SceneManager.LoadScene (State.GetNextSceneName());
	}
		
	// Update is called once per frame
	void Update () {
		
	}
}
