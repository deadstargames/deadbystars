﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {

	[SerializeField]
	public float WalkingSpeed = 2f;

	[SerializeField]
	public float RuningSpeed = 5f;

	private Rigidbody2D rb2d;
	private Crosshair crosshair;

    protected bool paused = false;

    public void OnPauseGame() {
        paused = true;
    }

    public void OnResumeGame() {
        paused = false;
    }

    void Start () {
		this.rb2d = GetComponent<Rigidbody2D> ();
		this.crosshair = FindObjectOfType<Crosshair> ();
	}

	void Update () {

	    if (!paused) {
	        UpdateMovement();
	        UpdateRotation();
	    }
	}

	private void UpdateMovement() {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector2 WalkingDiraction = new Vector2 (moveHorizontal, moveVertical);

		float speed = (Input.GetKey(KeyCode.LeftShift)) ? RuningSpeed : WalkingSpeed;

		rb2d.AddForce (WalkingDiraction.normalized * speed);
	}

	private void UpdateRotation() {
		var vectorFromPlayerToCroshair = crosshair.transform.position - this.transform.position;
		var endDiraction = vectorFromPlayerToCroshair.normalized;
		transform.right = Vector3.Lerp (transform.right, endDiraction, Time.deltaTime * 10f);
	}

}
